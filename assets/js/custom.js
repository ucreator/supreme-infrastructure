
  
// LOAN SLIDER
var slider = document.getElementById("myRange");
var output = document.getElementById("required_amount");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}

  //  MULTIPLICATION CALCULATOR ITALIAN HOME
no1 = document.getElementById('number1');
no2 = document.getElementById('number2');
mySum = document.getElementById('mySum');
button = document.getElementById('actionButton');
reset = document.getElementById('reset');

button.onclick = addNumbers;
reset.onclick = resetNumbers;

function addNumbers() {
  var a = getNumberA();
  var b = getNumberB();
   
  var c = a * b * 1450;
  postSumToPage(c);
}

function getNumberA() {
  var a  = parseInt(no1.value);
  return a;
}

function getNumberB() {
  var b = parseInt(no2.value);
  return b;
}

function postSumToPage(c) {
  var answer = c.toString();
  mySum.innerHTML = 'Rs. ' + answer;
}

function resetNumbers() {
  mySum.innerHTML = '';
}

  //  MULTIPLICATION CALCULATOR MEDITERRANIAN HOME
  noOneM = document.getElementById('numberOneM');
  noTwoM = document.getElementById('numberTwoM');
  mySumM = document.getElementById('mySumM');
  buttonM = document.getElementById('actionButtonM');
  resetM = document.getElementById('resetM');
  
  buttonM.onclick = addNumbersM;
  resetM.onclick = resetNumbersM; 
  
  function addNumbersM() {
    var a = getNumberAM();
    var b = getNumberBM();
     
    var c = a * b * 1450;
    postSumToPageM(c);
  }
  
  function getNumberAM() {
    var a  = parseInt(noOneM.value);
    return a;
  }
  
  function getNumberBM() {
    var b = parseInt(noTwoM.value);
    return b;
  }
  
  function postSumToPageM(c) {
    var answerM = c.toString();
    mySumM.innerHTML = 'Rs. ' + answerM;
  }
  
  function resetNumbersM() {
    mySumM.innerHTML = '';
  }

   //  MULTIPLICATION CALCULATOR MODERN HOME
   noOneMo = document.getElementById('numberOneMo');
   noTwoMo = document.getElementById('numberTwoMo');
   mySumMo = document.getElementById('mySumMo');
   buttonMo = document.getElementById('actionButtonMo');
   resetMo = document.getElementById('resetMo');
   
   buttonMo.onclick = addNumbersMo;
   resetMo.onclick = resetNumbersMo; 
   
   function addNumbersMo() {
     var a = getNumberAMo();
     var b = getNumberBMo();
      
     var c = a * b * 1300;
     postSumToPageMo(c);
   }
   
   function getNumberAMo() {
     var a  = parseInt(noOneMo.value);
     return a;
   }
   
   function getNumberBMo() {
     var b = parseInt(noTwoMo.value);
     return b;
   }
   
   function postSumToPageMo(c) {
     var answerMo = c.toString();
     mySumMo.innerHTML = 'Rs. ' + answerMo;
   }
   
   function resetNumbersMo() {
     mySumMo.innerHTML = '';
   }

    //  MULTIPLICATION CALCULATOR VAASTU HOME
  noOneV = document.getElementById('numberOneV');
  noTwoV = document.getElementById('numberTwoV');
  mySumV = document.getElementById('mySumV');
  buttonV = document.getElementById('actionButtonV');
  resetV = document.getElementById('resetV');
  
  buttonV.onclick = addNumbersV;
  resetV.onclick = resetNumbersV; 
  
  function addNumbersV() {
    var a = getNumberAV();
    var b = getNumberBV();
     
    var c = a * b * 1350;
    postSumToPageV(c);
  }
  
  function getNumberAV() {
    var a  = parseInt(noOneV.value);
    return a;
  }
  
  function getNumberBV() {
    var b = parseInt(noTwoV.value);
    return b;
  }
  
  function postSumToPageV(c) {
    var answerV = c.toString();
    mySumV.innerHTML = 'Rs. ' + answerV;
  }
  
  function resetNumbersV() {
    mySumV.innerHTML = '';
  }

// TYPES OF HOME FUNCTION
function showHomes(){
    
  $(document).ready(function() {
    $("#div_italian").hide();
      $("#btn_italian").click(function() {
        $("#div_home").hide();
        $("#div_mediterranian").hide();
        $("#div_modern").hide();
        $("#div_vaastu").hide();
        $("#div_italian").show();
      });
   });

  $(document).ready(function() {
  $("#div_mediterranian").hide();
    $("#btn_mediterranian").click(function() {
      $("#div_home").hide();
      $("#div_italian").hide();
      $("#div_modern").hide();
      $("#div_vaastu").hide();
      $("#div_mediterranian").show();
    });
  });
  
   $(document).ready(function() {
  $("#div_modern").hide();
    $("#btn_modern").click(function() {
      $("#div_home").hide();
      $("#div_italian").hide();
      $("#div_mediterranian").hide();
      $("#div_vaastu").hide();
      $("#div_modern").show();
    });
  });

  $(document).ready(function() {
    $("#div_vaastu").hide();
      $("#btn_vaastu").click(function() {
        $("#div_home").hide();
        $("#div_italian").hide();
        $("#div_mediterranian").hide();
        $("#div_modern").hide();
        $("#div_vaastu").show();
      });
    });

}